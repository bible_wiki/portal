import React, { useRef, useState } from 'react'
import logo from '../../logo.svg'
import { Button, Grid, Icon, Image, Input, Label, Popup } from 'semantic-ui-react'
import Connection from '../components/Connection';
import TelegramLogin from '../components/buttons/TelegramLogin'
import GoogleLogin from '../components/buttons/GoogleLogin'
import SmsLogin from '../components/buttons/SmsLogin'

const Profile = ({ leftRight, topBottom }) => {
    const [ loadImage, setLoadImage ] = useState(false)
    const hiddenFileInput = useRef(null)
    const profile = useRef(null)

    const saveImage = (e) => {
        setLoadImage(true)
        console.log('send it:')
        console.log(e.target.files[0])
    }

    const returnImage = (newLogo) => {
        profile.current.src = newLogo
    }

    return <div style={{ marginLeft: leftRight, marginRight: leftRight, marginTop: topBottom, marginBottom: topBottom }}>
        <Grid>
            <Grid.Row>
                <Grid.Column>
                    <p>Status: <Popup
                        content='Dein Konto wurde von dir abgeschlossen. Ist aber noch nicht bestätigt worden.'
                        trigger={<Label as='a' color='green'>Abgeschlossen</Label>}
                        position='top center' />
                    </p>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Input iconPosition='left' placeholder='Dein Benutzername' autoFocus transparent fluid>
                        <Icon name='at' />
                        <input />
                    </Input>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
                <Grid.Column width={6}>
                    <Image
                        style={loadImage ? null : {cursor: 'pointer'}}
                        onClick={loadImage ? null : () => hiddenFileInput.current.click()}
                        disabled={loadImage}
                        src={logo}
                        ref={profile}
                        size='tiny'
                        verticalAlign='middle'
                        circular />
                    <input
                        onClick={(e) => e.target.value = null}
                        onChange={(e) => saveImage(e)}
                        type="file"
                        accept="image/*"
                        className='upload'
                        ref={hiddenFileInput}
                        hidden />
                </Grid.Column>
                <Grid.Column>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <Input placeholder='Dein Rufname' transparent fluid />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Input placeholder='Deine Wurzeln' transparent fluid />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{padding:0}}>
                <Grid.Column>
                    <Connection
                        tag='tg'
                        link='@michi'
                        button={<TelegramLogin size='medium' link />} />
                    <Connection
                        tag='go'
                        link='michi@gmail.com'
                        button={<GoogleLogin size='medium' link />} />
                    <Connection
                        tag='sms'
                        link='077 464 49 11'
                        button={<SmsLogin size='medium' link />} />
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                <Button onClick={null} color='blue' size='large' fluid >
                        Zurück zu {'?redirect='}
                </Button>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </div>
}

export default Profile