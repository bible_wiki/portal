import React from 'react'
import TelegramLogin from '../components/buttons/TelegramLogin'
import GoogleLogin from '../components/buttons/GoogleLogin'
import SmsLogin from '../components/buttons/SmsLogin'
import { Card } from 'semantic-ui-react'

const Login = ({ leftRight, topBottom }) => {
    return <div style={{textAlign:'center',marginLeft:leftRight,marginRight:leftRight,marginTop:topBottom,marginBottom:topBottom}}>
        <Card.Content style={{marginBottom:20}}>
            <TelegramLogin size='massive' />
        </Card.Content>
        <Card.Content style={{marginBottom:20}}>
            <GoogleLogin size='massive' />
        </Card.Content>
        <Card.Content>
            <SmsLogin inline />
        </Card.Content>
    </div>
}

export default Login