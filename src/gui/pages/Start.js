import React from 'react'
import logo from '../../logo.svg'
import profile from '../../profile.svg'
import { Card, Grid, Icon, Image, Segment } from 'semantic-ui-react'
import CenterWindow from '../CenterWindow'

const Start = () => {

    return <Grid style={{height: '100vh', marginTop:0}}>
        <Grid.Row columns={1} style={{ height:80,padding:0 }}>
            <Grid.Column style={{height:80}}>
                <Segment size='big' style={{height:80}}>
                    <Grid>
                        <Grid.Row columns={2}>
                            <Grid.Column verticalAlign='middle'>
                                <Image src={logo} size='mini' floated='left' />
                                <h2 style={{ marginTop: 5 }}>BibleWiki Portal</h2>
                            </Grid.Column>
                            <Grid.Column verticalAlign='middle' textAlign='right'>
                                <span style={{ display: 'inline-block', marginTop: 5 }}>Name</span>
                                <Image src={profile} size='mini' floated='right' style={{cursor:'pointer'}} circular />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>
            </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2} style={{padding:0, height:'calc(100% - 80px)'}}>
            <Grid.Column width={12} style={{height:'100%'}}>
                <Grid style={{padding:40}}>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <Card as='a' href='//edit.biblewiki.one' target='_blank' style={{padding:10, color:'#555'}} fluid>
                                <Card.Header as='h2' style={{margin:10}}>
                                    Edit
                                    <Icon name='external alternate' style={{float:'right'}} />
                                </Card.Header>
                                <Card.Content>
                                    Hier ensteht das BibleWiki. Erfasse dein erster Eintrag.
                                </Card.Content>
                                <Card.Content extra>
                                    edit.biblewiki.one
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                        <Grid.Column>
                            <Card as='a' href='//read.biblewiki.one' target='_blank' style={{padding:10, color:'#555'}} fluid>
                                <Card.Header as='h2' style={{margin:10}}>
                                    Read
                                    <Icon name='external alternate' style={{float:'right'}} />
                                </Card.Header>
                                <Card.Content>
                                    Hier kannst du die Bibel lesen.
                                </Card.Content>
                                <Card.Content extra>
                                    read.biblewiki.one
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Grid.Column>
            <Grid.Column width={4}>
                <CenterWindow
                    path2="login"
                    title="Anmelden"
                    subtitle='mit'
                    sidebar
                    messages={[
                        {
                            type: 'error',
                            name: 'loginFailed',
                            text: 'Das Anmelden ist fehlgeschlagen. Versuch es noch einmal.'
                        }
                    ]} />
            </Grid.Column>
        </Grid.Row>
    </Grid>
}

export default Start