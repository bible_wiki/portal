import React, { Fragment, useEffect, useState } from 'react'
//import { useHistory } from 'react-router-dom'
import { Button, Grid, Icon, Input } from 'semantic-ui-react'

const Sms = ({ leftRight, topBottom }) => {
    //const history = useHistory()
    const [isValidNumber, setIsValidNumber] = useState(true)
    const [isNumber, setIsNumber] = useState(null)
    const [sended, setSended] = useState(false)
    const [numberUsername, setNumberUsername] = useState('')
    const [code, setCode] = useState('')
    const [codePlaceholder, setCodePlaceholder] = useState('Schau auf deinem Natel nach')
    const [numberUsernamePlaceholder, setNumberUsernamePlaceholder] = useState('Hast du einen Benutzernamen?')

    const numberUsernameFinish = () => {
        checkNumber()
        if (isNumber) {
            // TODO check false
            if (isValidNumber) {
                setSended(true)
                //history.push('?number=+41' + numberUsername.replaceAll(' ', ''))
                console.log('send it: +41 ' + numberUsername.replaceAll(' ', ''))
                document.getElementsByClassName('codeSended')[0].classList.add('show')
            }
        } else if (isNumber !== null) {
            setSended(true)
            //history.push('?username=' + numberUsername)
            document.getElementsByClassName('codeSended')[0].classList.add('show')
        }
    }
    const codeFinish = () => {
        console.log('send it: ' + code)
    }
    
    const codeChange = (e) => {
        const val = e.target.value

        if (!numb(val) && val.length <= 6) {
            setCode(val.toUpperCase())
        }
    }

    const checkNumber = () => {
        setIsValidNumber(numberUsername.replaceAll(' ', '').length === 9)
    }
    const numberUsernameChange = (e) => {
        const val = e.target.value

        if (val.length === 0) {
            setIsNumber(null)
            setNumberUsername('')
        } else if (val.length === 1) {
            if (numb(val)) {
                setIsNumber(true)
                setNumberUsername(val)
            } else if (alphabet(val)) {
                setIsNumber(false)
                setNumberUsername(val)
            } else {
                setIsNumber(null)
                setNumberUsername('')
            }
        } else {
            if (isNumber) {
                if (numb(val)) {
                    setNumber(val)
                } else {
                    setNumber(val.slice(0, -1))
                }
            } else {
                if (alphabet(val)) {
                    setUsername(val)
                }
            }
        }
    }

    const numb = (val) => val.match(/^[ 0-9]+$/)
    const alphabet = (val) => val.match(/^[A-Za-z]+$/)

    const setUsername = (val) => {
        val.length <= 8 && setNumberUsername(val.toLowerCase())
    }

    const setNumber = (val) => {
        const valClean = val.replaceAll(' ', '')
        const part1 = valClean.slice(0, 2) || valClean.slice(0, 1)
        const part2 = valClean.slice(2, 5) || valClean.slice(2, 4) || valClean.slice(2, 3)
        const part3 = valClean.slice(5, 7) || valClean.slice(5, 6)
        const part4 = valClean.slice(7, 9) || valClean.slice(7, 8)

        let formated = part1
        part2 && (formated += '  ' + part2)
        part3 && (formated += '  ' + part3)
        part4 && (formated += '  ' + part4)

        setNumberUsername(formated)
        isValidNumber || checkNumber()
    }

    useEffect(() => {        
        const numberUsernamePlaceholders = ['Hast du einen Benutzernamen?', 'Kannst ihn hier eingeben', 'Sonst einfach deine Nummer']
        const codePlaceholders = ['Schau auf deinem Natel nach', 'Denn Code den du dort findest', 'Musst du hier eintippen' ]
        let step = 0

        function setPlaceholder(){
            setTimeout(() => {
                setNumberUsernamePlaceholder(numberUsernamePlaceholders[step])
                setCodePlaceholder(codePlaceholders[step])
                step < numberUsernamePlaceholders.length - 1 ? step++ : step = 0
                clearTimeout(setPlaceholder)
                setPlaceholder()
            }, 2000)
        }
        setPlaceholder()
    }, [ setNumberUsernamePlaceholder, setCodePlaceholder ])

    return <div style={{ marginLeft: leftRight, marginRight: leftRight, marginTop: topBottom, marginBottom: topBottom }}>
        <Grid>
            <Grid.Row columns={1}>
                <Grid.Column style={{padding:'0 5px'}}>
                    {sended ?
                        <Input placeholder={codePlaceholder} onChange={(e) => codeChange(e)} value={code} className='code' autoFocus transparent fluid /> :
                        <Fragment>
                            <Input className='number' error={!isValidNumber} placeholder={numberUsernamePlaceholder} onChange={(e) => numberUsernameChange(e)} value={numberUsername} autoFocus transparent fluid>
                                {isNumber !== null && <span style={{ fontWeight: 900, color: '#B8B8B8', paddingRight: 10 }}>{isNumber ? '+41' : <Icon name='at' />}</span>}
                                <input />
                            </Input>
                        </Fragment>
                    }
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                {sended ?
                    <Button onClick={codeFinish} color='blue' size='large' fluid >
                        Anmelden
                </Button> :
                    <Button onClick={numberUsernameFinish} color='blue' size='large' fluid >
                        Code senden
                </Button>
                }
            </Grid.Row>
        </Grid>
    </div>
}

export default Sms