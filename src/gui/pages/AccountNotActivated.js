import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Grid } from 'semantic-ui-react'

// DELETE
const AccountNotActivated = ({ leftRight, topBottom }) => {
    const history = useHistory()

    return <div style={{marginLeft:leftRight,marginRight:leftRight,marginTop:topBottom,marginBottom:topBottom}}>
        <Grid>
            <Grid.Row columns={1}>
                <Grid.Column textAlign='center'>
                    <p>
                        Dein Konto muss noch freigeschaltet werden. Sobald es soweit ist, werde ich dich weiterleiten.
                    </p>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Button onClick={() => history.push('/profile')} color='blue' size='large' fluid >
                        Mein Profil
                    </Button>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </div>
}

export default AccountNotActivated