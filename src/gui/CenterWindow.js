import React from 'react'
import { useParams } from 'react-router-dom'
import { Card, Grid, Header } from 'semantic-ui-react'
import AccountNotActivated from './pages/AccountNotActivated'
import Login from './pages/Login'
import Profile from './pages/Profile'
import Sms from './pages/Sms'

const CenterWindow = ({ path2, title, subtitle, messages, sidebar }) => {
    let { path } = useParams()
    if (!path) (path = path2)

    return <div style={{height: sidebar ? '100%' : '100vh',display:'flex',alignItems:'center',justifyContent:'center',backgroundColor:'#4d4d4d'}}>
        <Grid>
            <Grid.Row columns={1} centered>
                <Header as='h1' style={{color:'white',fontSize:'25pt',marginBottom:0}}>
                        {title}
                    <Header.Subheader style={{color:'white'}}>
                        {subtitle}
                    </Header.Subheader>
                </Header>
            </Grid.Row>
            <Grid.Row>
                <Card centered>
                    {messages && messages.map(mes => {
                        return <Card.Content
                            style={{
                                display:'none',
                                color: mes.type === 'info' ? 'green' : mes.type === 'error' ? 'red' : 'white',
                                backgroundColor: mes.type === 'info' ? 'rgba(0, 255, 0, .2)' : mes.type === 'error' ? 'rgba(255, 0, 0, .2)' : 'grey'
                            }}
                            className={[mes.type, mes.name].join(' ')}
                            key={mes.name}
                            textAlign='center'
                            extra>
                            {mes.text}
                        </Card.Content>
                    })}
                    <Card.Description style={{margin:20}}>
                        {
                            path === 'login' ? <Login leftRight={10} topBottom={10} /> :
                            path === 'register' ? <Profile leftRight={10} topBottom={10} /> :
                            path === 'profile' ? <Profile leftRight={10} topBottom={10} /> :
                            path === 'sms' ? <Sms leftRight={20} topBottom={10} /> :
                            path === 'account-not-activated' ? <AccountNotActivated leftRight={10} topBottom={10} /> :
                            'Fehlercode: 404'
                        }
                    </Card.Description>
                </Card>
            </Grid.Row>
        </Grid>
    </div>
}

export default CenterWindow