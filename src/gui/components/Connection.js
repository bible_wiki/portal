import React, { useState } from 'react'
import { Icon, Message, Transition } from 'semantic-ui-react'

const Connection = ({ tag, link, button }) => {

    const [ visible, setVisible ] = useState(true)
    const [ visibleButton, setVisibleButton ] = useState(false)
    const linked = false

    const dismiss = () => {
        setVisible(false)
    }

    const showButton = () => setVisibleButton(true)

    const title = () => {
        switch (tag) {
            case 'tg':
                return 'Telegram'
            case 'go':
                return 'Google'
            case 'sms':
                return 'SMS'
            default:
                return 'Keine Verknüpfung'
        }
    }

    const icon = () => {
        switch (tag) {
            case 'tg':
                return 'telegram plane'
            case 'go':
                return 'google'
            case 'sms':
                return 'phone'
            default:
                return 'cog'
        }
    }

    const color = () => {
        switch (tag) {
            case 'tg':
                return 'blue'
            case 'go':
                return 'red'
            case 'sms':
                return 'yellow'
            default:
                return 'grey'
        }
    }

    return <Transition visible={visible} animation='scale' duration={500}>
        <div style={{marginBottom:10}}>
            <Message
                onDismiss={linked ? () => dismiss() : null}
                color={color()}
                className='linked'
                size='small'
                floating
                icon >
                <Icon name={icon()} />
                <Message.Content>
                <Message.Header>{title()}</Message.Header>
                {linked ?
                    link :
                    visibleButton ?
                    <div style={{marginTop:10}}>
                        {button}
                    </div> :
                    <div onClick={showButton} style={{cursor: 'pointer'}}>
                        <u>Verknüpfen</u> <Icon name='arrow right' />
                    </div>
                }
                </Message.Content>
            </Message>
        </div>
    </Transition>
}

export default Connection