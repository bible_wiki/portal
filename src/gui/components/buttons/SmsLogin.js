import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon } from 'semantic-ui-react'

const SmsLogin = ({ size, link, inline }) => {
    const history = useHistory()

    return inline ?
        <Link to='/login/sms' style={{ display: 'block', textAlign: 'center', textDecoration: 'underline' }}>
            Oder mit einem SMS-Code anmelden
        </Link> :
        <Button onClick={() => history.push('/login/sms')} color='yellow' size={size} fluid>
            <Icon name={link ? 'linkify' : 'phone'} />
            {link ? 'Anmelden' : 'SMS'}
        </Button>
}

export default SmsLogin