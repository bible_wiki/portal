import React, { Fragment } from 'react'
import { Button, Icon } from 'semantic-ui-react'
// https://core.telegram.org/widgets/login
// https://github.com/hprobotic/react-telegram-login
// https://stackoverflow.com/a/63593384

const TelegramLogin = ({ size, link }) => {

    const login = () => window.Telegram.Login.auth(
        { bot_id: '1459099975', request_access: true },
        (data) => {
            if (!data) {
                console.log('failed')
            } else {
                // Here you would want to validate data like described there https://core.telegram.org/widgets/login#checking-authorization
                console.log(data)
            }
        }
    )
    
    const logout = () => {
        console.log('logout tg')
    }

    return <Fragment>
        <Button onClick={() => login()} color='blue' size={size} fluid>
            <Icon name={link ? 'linkify' : 'telegram plane'} />
            {link ? 'Anmelden' : 'Telegram'}
        </Button>
        {/*<TelegramLoginButton
            botName='BibleWikiBot'
            dataAuthUrl='http://api.biblewiki.one/auth/tg/login'
            requestAccess='write'
            cornerRadius={2}
            usePic={false}
            lang='de'
            buttonSize='large' />*/}
    </Fragment>
}

export default TelegramLogin