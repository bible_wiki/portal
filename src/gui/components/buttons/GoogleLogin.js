import React from 'react'
import GoogleLoginButton from 'react-google-login'
import { Button, Icon } from 'semantic-ui-react'
// https://github.com/anthonyjgrove/react-google-login

const GoogleLogin = ({ size, link }) => {

    const login = (response, success) => {
        if (!success) {
            console.log('failed')
        } else {
            // token = aJgYfh8207hpZFjUyMZT9hPx
            console.log(response)
        }
    }

    const logout = () => {
        /*var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('logout go')
        })*/
    }

    return <GoogleLoginButton
        clientId='425472258711-9othq72etpc300cfc0hpqsmomf4vt4f1.apps.googleusercontent.com'
        render={renderProps => (
            <Button color='red' size={size} onClick={renderProps.onClick} disabled={renderProps.disabled} fluid>
                <Icon name={link ? 'linkify' : 'google'} />
                {link ? 'Anmelden' : 'Google'}
            </Button>
        )}
        buttonText='Google'
        uxMode='redirect'
        redirectUri='http://api.biblewiki.one/auth/go/login'
        fetchBasicProfile={true}
        onSuccess={(response) => login(response, true)}
        onFailure={(response) => login(response, false)}
        cookiePolicy={'single_host_origin'}
        scope='josua.koenig99@gmail.com'
        theme='dark' />
}

export default GoogleLogin