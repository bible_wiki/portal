import React, { useEffect, useState } from 'react'
import { Icon, Message, Transition } from 'semantic-ui-react'

const MessageItem = ({ icon, title, text, onClick }) => {

    const [ visible, setVisible ] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setVisible(true)
        }, 1000)
    })

    return <Transition visible={visible} animation='fade left' duration={1000}>
        <div style={{cursor:onClick ? 'pointer' : 'default',position:'fixed',bottom:30,right:30}} onClick={onClick}>
            <Message
                as='a'
                onDismiss={() => setVisible(false)}
                className='linked'
                color={'green'}
                size='small'
                floating
                icon >
                {icon && <Icon name={icon} />}
                <Message.Content>
                    <Message.Header>{title}</Message.Header>
                    {text}
                </Message.Content>
            </Message>
        </div>
    </Transition>
}

export default MessageItem