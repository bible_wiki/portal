import React, { Fragment } from 'react'
import './App.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Start from './gui/pages/Start'
import CenterWindow from './gui/CenterWindow'
import MessageContainer from './gui/components/message/MessageContainer'

export default function App() {
    return (
        <Fragment>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Start />
                    </Route>
                    <Route path="/login/sms">
                        <CenterWindow
                            path2="sms"
                            title="SMS"
                            subtitle="Mit einem Code anmelden"
                            messages={[
                                {
                                    type: 'error',
                                    name: 'phoneNumberFalse',
                                    text: 'Die Nummer muss gültig sein.'
                                },
                                {
                                    type: 'info',
                                    name: 'codeSended',
                                    text: 'Der Code wurde versendet.'
                                }
                            ]} />
                    </Route>
                    <Route path="/login">
                        <CenterWindow
                            path2="login"
                            title="Anmelden"
                            subtitle='mit'
                            messages={[
                                {
                                    type: 'error',
                                    name: 'loginFailed',
                                    text: 'Das Anmelden ist fehlgeschlagen. Versuch es noch einmal.'
                                }
                            ]} />
                    </Route>
                    <Route path="/register">
                        <CenterWindow
                            path2="register"
                            title="Registrieren"
                            messages={[
                                {
                                    type: 'error',
                                    name: 'userExists',
                                    text: 'Dieser Benutzername existiert schon.'
                                }
                            ]} />
                    </Route>
                    <Route path="/profile">
                        <CenterWindow
                            path2="profile"
                            title="Profil"
                            messages={[
                                {
                                    type: 'error',
                                    name: 'userExists',
                                    text: 'Dieser Benutzername existiert schon.'
                                }
                            ]} />
                    </Route>
                    <Route path="/account-not-activated">
                        <CenterWindow
                            path2="account-not-activated"
                            title="Bitte um Geduld"
                            messages={[
                                {
                                    type: 'error',
                                    name: 'userExists',
                                    text: 'Dieser Benutzername existiert schon.'
                                }
                            ]} />
                    </Route>
                    <Route path="/:path" >
                        <CenterWindow
                            title="Seite nicht gefunden" />
                    </Route>
                </Switch>
            </Router>
            <MessageContainer />
        </Fragment>
    )
}